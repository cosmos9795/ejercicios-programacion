#ADRIAN PEREZ DE LA VEGA 14-08-2020
#funcion principal 
def main():
    numList = []
    for i in range(0,3):
        numList.append(validacion_num(i+1))
    desig_triang(numList[0],numList[1],numList[2])
#teorema de desigualdad de triangulos
def desig_triang(x,y,z):
    # a<b+c | b < a+c | c<a+b
    if((x<y+z) and (y < x+z) and (z < x+y)):
        print("SI, se puede formar un triangulo")
    else:
        print("NO se puede formar un triangulo")
#Valida si es un numero entero
def validacion_num(lado):
    while(1):
        print("Ingresa el lado: ",lado)
        try:
            num = int(input())
            return num
        except:
            print("Entrada Invalida, Intente de nuevo\n")
            
#lamada a la función principal
main()