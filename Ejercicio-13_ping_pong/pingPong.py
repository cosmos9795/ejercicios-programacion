#Constantes
INICIO = 1
FINAL  = 100

#Funcion principal
def main():
    
    for i in range(INICIO,FINAL+1):
        
        if ((i % 3 == 0) and (i % 5 == 0)):
            print("\tping-pong")
        elif (i % 3 == 0):
            print(" \tping")
        elif (i % 5 == 0):
            print("\tpong")
        else:
            print("\t",i)
#Llamada a la función principal
main()