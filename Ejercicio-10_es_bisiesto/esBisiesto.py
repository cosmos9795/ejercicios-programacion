# ADRIAN PEREZ DE LA VEGA 14-08-2020
#Función principal
def main():
    #Diccionario con los meses
    meses={0:'Enero',1:'Febrero',2:'Marzo',3:'Abril',4:'Mayo',5:'Junio',6:'Julio',
           7:'Agosto',8:'Septiembre',9:'Octubre',10:'Noviembre',11:'Diciembre'}
    
    while(1):
        print("Ingresa un numero entre 0 y 11")
        #Manejo de excepcion por si no cumple las caracteristicas 
        try:
            opt = int(input())
            if ((opt >=0) and (opt<=11)):
                break
        except:
            
            print("Entrada invalida, Intente de nuevo")
    print("Mes: ",meses[opt])
#Llamada a la función principal
main()