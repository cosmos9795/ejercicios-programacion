/*ADRIAN P�REZ DE LA VEGA 13-08-2020*/
#include<stdio.h>
#include<stdlib.h>
#define RANGO_INI 1
#define RANGO_FIN 100
//Definici�n de Funciones
int calcula_pares(int numero);

// Funci�n principal main
int main(){
	printf("\t Numeros pares \n");
	printf("\t");
	calcula_pares(RANGO_INI); //inicia de 0
	return 0;
}
//Funci�n recursiva: Imprime los numeros pares dependiendo el rango
int calcula_pares(int numero){
	if (numero%2 != 0){
		calcula_pares(numero + 1);
	}
	else{
		if(numero >= RANGO_FIN){
			printf("%i",numero);
			return 0;
		}
		else{
			printf("%i\t",numero);
			calcula_pares(numero + 2);
		}
	}
	
}
