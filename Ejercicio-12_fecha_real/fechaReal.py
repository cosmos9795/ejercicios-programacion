# ADRIAN PEREZ DE LA VEGA 14-08-2020
#Función principal
def main():
    #      Ene  Feb  Mar  Abr  May  Jun  Jul  Ago  Sep  Oct  Nov  Dic
    meses=[31,  28,  31,  30,  31,  30,  31,  31,  30,  31,  30,  31]
    
    while(1):
        print("Ingresa una fecha: Formato dd-mm-yyyy")
        anio = input()
        if (comprueba_format(anio) == True):
            break
    
    if (anio_bisiesto(int(anio[6:])) == True):
        meses[1] = 29
    #   si el mes existe y si el dia existe
    if((comprueba_mes(int(anio[3:5]))== True) and (comprueba_dia(int(anio[0:2]),meses[int(anio[3:5])-1])==True)):
        print("\nLa fecha",anio,"es correcta")
    else:
        print("\nLa fecha",anio,"es incorrecta")
        
#Valida el formato
def comprueba_format(datos):
    #Formato dd-mm-yyyy : Tamaño = 10
    if((len(datos)==10) and (datos[2]=='-') and (datos[5]=='-') ):
        #Manejo de excepcion
        try: 
            int(datos[0:2])
            int(datos[3:5])
            int(datos[6:])
            return True
        except:
            return False
            
    else:
        return False
#Verifica si es año bisiesto
def anio_bisiesto(anio):
    if(((anio % 400 == 0))or((anio % 4 == 0) and (anio % 100 != 0))):
        return True
    else:
        return False
#Comprueba si el mes es verdadero
def comprueba_mes(mes):
    if ((mes > 0) and (mes <13)):
        return True
    return False
#Comprueba si el dia es verdadero, recibe(dia y el mes de la lista meses[])
def comprueba_dia(dia,mes):
    if ((dia >0) and (dia <= mes)):
        return True
    else:
        return False

#Llamada a la función principal
main()