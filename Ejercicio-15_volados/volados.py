# ADRIAN PEREZ DE LA VEGA 18-08-2020

#Librerias
from random import randint
#Funcion principal
def main_volados():

    while(1):
        print("\tVOLADOS")
        print("[1]---Jugar")
        print("[2]---Salir")
        try:
            opt = int(input())
            if (opt == 1):
                break
            elif(opt == 2):
                print("Juego Finalizado")
                return 0
            else:
                print("Solo 1 o 2")
        except:
            print("Entrada invalida, intente de nuevo")
    
    inicia_juego(500,500)
            

#Funcion inicia el juego                 
def inicia_juego(bolsaPC,bolsaUsu):
    moneda = ["AGUILA","SOL"]
    result = []
    acumPC  = 0
    acumUsu = 0
    apuestaPC = 0
    apuestaUsu = 0
    acumApuesPC  = 0
    acumApuesUsu = 0
    
    for i in range(1,4):
        print("\t========Ronda",i,"========")
        #Respuesta
        tirada = moneda[randint(0,1)]
        
        #apuesta de la PC
        apuestaPC  = randint(20,100)
        if (apuestaPC > bolsaPC):
            print("Apuesta muy alta PC, solo cuenta con ",bolsaPC)
            return 0
        acumApuesPC += apuestaPC
        bolsaPC -= apuestaPC
        caraMonedaPC  = moneda[randint(0,1)]
        
        #Apuesta del usuario
        apuestaUsu = pide_apuesta()
        if (apuestaUsu > bolsaUsu):
            print("Apuesta muy alta Usuario, solo cuenta con ",bolsaUsu)
            return 0
        acumApuesUsu += apuestaUsu
        bolsaUsu -= apuestaUsu
        caraMonedaUsu = selecciona_cara(moneda)
        
        #Acumulador para comparar los lanzamientos
        if(caraMonedaPC==tirada):
            acumPC += 1
        if (caraMonedaUsu==tirada):
            acumUsu += 1
        print(" \tRESULTADO DE LANZAMIENTO ",i," : ",tirada)
        print("\tPC: ",caraMonedaPC,"| Apuesta: ",apuestaPC,"| bolsa: ",bolsaPC)
        print("\tUSUARIO: ",caraMonedaUsu,"| Apuesta: ",apuestaUsu,"| bolsa",bolsaUsu)
        
    # comprueba ganador
        if ((acumPC == 2) and (acumUsu==0)):
            print("\t==Gano la PC==")
            bolsaPC += acumApuesPC+ acumApuesUsu
            print("\t PC : Total Apuesta ",acumApuesPC,"| Bolsa $ ",bolsaPC)
            print("\t USUARIO : Total Apuesta ",acumApuesUsu,"| Bolsa $ ",acumApuesUsu)
            return 0
        if ((acumUsu == 2) and (acumPC == 0)):
            print("\t==Gano el USUARIO==")
            bolsaUsu += acumApuesPC+ acumApuesUsu
            print("\t PC : Total Apuesta ",acumApuesPC,"| Bolsa $ ",bolsaPC)
            print("\t USUARIO : Total Apuesta ",acumApuesUsu,"| Bolsa $ ",acumApuesUsu)
            return 0
    if(acumPC > acumUsu):
        print("\t==Gano la PC==")
        bolsaPC += acumApuesPC+ acumApuesUsu
        print("\t PC : Total Apuesta ",acumApuesPC,"| Bolsa $ ",bolsaPC)
        print("\t USUARIO : Total Apuesta ",acumApuesUsu,"| Bolsa $ ",bolsaUsu)
    if(acumUsu > acumPC):
        print("\t==Gano el USUARIO==")
        bolsaUsu += acumApuesPC+ acumApuesUsu
        print("\t PC : Total Apuesta ",acumApuesPC,"| Bolsa $ ",bolsaPC)
        print("\t USUARIO : Total Apuesta ",acumApuesUsu,"| Bolsa $ ",bolsaUsu)
            
    if(acumPC == acumUsu):
        print("\t==EMPATE NADIE PIERDE==")
        bolsaPC += acumApuesPC
        bolsaUsu += acumApuesUsu
        print("\t PC : Total Apuesta ",acumApuesPC,"| Bolsa $ ",bolsaPC)
        print("\t USUARIO : Total Apuesta ",acumApuesUsu,"| Bolsa $ ",bolsaUsu)
        

    return 

#Pide la apuesta al usuario y comprueba que sea correcta 
def pide_apuesta():
    print("Ingresa la apuesta entre 20 y 100")
    while(1):
        try:
            apuestaUsu = int(input())
            apuestaUsu = int(apuestaUsu)
            if ((apuestaUsu >= 20) and (apuestaUsu <= 100)):
                return apuestaUsu
            else:
                print("Solo entre 20 y 100, intente de nuevo")
        except:
            print("apuesta invalida, intente de nuevo")

#Pide la cara de la moneda y comprueba si es correcta la entrada
def selecciona_cara(moneda):
    
    while(1):
        print("Ingresa la cara de la moneda ")
        caraMonedaUsu = input()
        if (type(caraMonedaUsu)==str):
            caraMonedaUsu=caraMonedaUsu.upper()
            if ((caraMonedaUsu == moneda[0]) or (caraMonedaUsu ==moneda[1])):
                return caraMonedaUsu
        
        print("Entrada invalida, solo Aguila o Sol")  

#Llamada a la función principal
main_volados()