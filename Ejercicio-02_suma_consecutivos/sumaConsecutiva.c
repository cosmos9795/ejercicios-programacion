/*ADRIAN P�REZ DE LA VEGA 13-08-2020*/
#include<stdio.h>
#include<stdlib.h>
//Definici�n de Funciones
int suma_cosecutivos(int numero, int sumConsec);
//Funcion pricipal main
int main(){
	int num = 0;
	while((num < 1) or (num > 50)){
		printf("Ingresa un numero entre 1 y el 50 : ");
		scanf("%i",&num);
	}
	suma_cosecutivos(num,0);
	return 0;
}

//Funci�n recursiva: Suma los numeros consecutivos 1 hasta el numero de entrada
int suma_cosecutivos(int numero,int sumConsec){
	if (numero == 0){
		printf("Suma consecutiva: %i",sumConsec);
		return 0;
	}
	else{
		suma_cosecutivos(numero-1,sumConsec+numero);
	}	
}
