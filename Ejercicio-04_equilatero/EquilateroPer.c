/*ADRIAN P�REZ DE LA VEGA 13-08-2020*/
#include<stdio.h>
#include<stdlib.h>
//Definici�n de Funciones
void calcula_perimetro(float lado);
//Funci�n principal main
int main(){
	float lado = 0.0;
	printf("\t TRIANGULO EQUILATERO \n");
	
	//lado debe ser mayor que cero
	while(lado <= 0){
		printf("\tIngresa la medida de algun lado del triangulo\n");
		printf("\tLado : ");
		scanf("%f",&lado);
	}
	calcula_perimetro(lado);
	return 0;
}
//Funci�n calcula perimetro 
void calcula_perimetro(float lado){
	// (Lado + Lado + Lado)   � (Lado * 3)
	printf("\tPERIMETRO : %.2f",lado *3 );	
}
