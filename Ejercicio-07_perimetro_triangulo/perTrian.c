/*ADRIAN P�REZ DE LA VEGA 13-08-2020*/
#include "perTrian.h"

// Funciones para calcular los perimetros de triangulos
void perimetro_equilatero(){
	float lado = 0.0;
	printf("\t TRIANGULO EQUILATERO \n");
	
	while(lado <= 0){
		printf("\tIngresa la medida de algun lado del triangulo\n");
		printf("\tLado : ");
		scanf("%f",&lado);
	}
	printf("\tPERIMETRO : %.2f",lado *3 );
}

void perimetro_isosceles(){
	float lado1 = 0.0;
	float lado2 = 0.0;
	printf("\t TRIANGULO ISOSCELES \n");
	
	while(lado1 <= 0){
		printf("\tIngresa la medida del lado 1 (base)\n");
		printf("\tLado 1 : ");
		scanf("%f",&lado1);
	}
	while((lado2<=0) or (lado2 == lado1)){
		if(lado1 == lado2){
			printf("\n\tlado 2 debe ser diferente a lado 1\n");
		}
		printf("\tIngresa la medida del lado 2 \n");
		printf("\tLado 2 : ");
		scanf("%f",&lado2);
	}
	
	printf("\tPERIMETRO : %.2f",lado1 +(lado2 * 2) );
}

void perimetro_escaleno(){
	float lado1 = 0.0;
	float lado2 = 0.0;
	float lado3 = 0.0;
	printf("\t TRIANGULO ISOSCELES \n");
	
	while(lado1 <= 0){
		printf("\tIngresa la medida del lado 1 \n");
		printf("\tLado 1 : ");
		scanf("%f",&lado1);
	}
	
	while((lado2<=0) or (lado2 == lado1)){
		if(lado1 == lado2){
			printf("\n\tlado 2 debe ser diferente a lado 1\n");
		}
		printf("\tIngresa la medida del lado 2 \n");
		printf("\tLado 2 : ");
		scanf("%f",&lado2);
	}
	
	while((lado3<=0) or (lado3 == lado1) or (lado3 == lado2)){
		if(lado3 == lado1){
			printf("\n\tlado 3 debe ser diferente a lado 1\n");
		}
		if(lado3 == lado2){
			printf("\n\tlado 3 debe ser diferente a lado 2\n");
		}
		printf("\tIngresa la medida del lado 3 \n");
		printf("\tLado 3 : ");
		scanf("%f",&lado3);
	}
	printf("\tPERIMETRO : %.2f",lado1 + lado2 + lado3 );
}
