/*ADRIAN P�REZ DE LA VEGA 13-08-2020*/
#include<stdio.h>
#include<stdlib.h>
#include "perTrian.c" //A�ADIR ARCHIVOS

int main(){
	int opcion = 0;
	while(opcion != 4){
		printf("\tPERIMETRO DE TRIANGULOS\n");
		printf("Ingresa una opcion\n");
		printf("[1] ---- Equilatero\n");
		printf("[2] ---- Isosceles\n");
		printf("[3] ---- Escaleno\n");
		printf("[4] ---- Salir\n");
		scanf("%i",&opcion);
		
		switch(opcion){
			case 1:
				perimetro_equilatero();
				break;
			case 2:
				perimetro_isosceles();
				break;
			case 3:
				perimetro_escaleno();
				break;
			case 4:
				printf("Fin del programa\n");
				system("pause");
				opcion = 4;
				break;
			default:
				printf("Entrada invalida\n");
				break;
		}
	}
	
	
	return 0;
}


