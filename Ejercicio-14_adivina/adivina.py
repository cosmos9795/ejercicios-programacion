# ADRIAN PEREZ DE LA VEGA 14-08-2020

#Librerias
from random import randint

#Funcion principal
def main():
    numAleat = randint(1,100)
    intentos  = 5
    print("\t|Adivina el numero entre 1 y 100|\n")
    while(intentos != 0):
        print("\tTienes",intentos,"oportunidades\n")
        try:
            opt = int(input())
        except:
            print("\tSolo numero enteros, PIERDES UNA oportunidad")
            opt = 0
        if(opt == numAleat):
            break;
        
        intentos-=1
    
    if(intentos > 0):
        print("GANASTE, el numero es: ",numAleat)
    else:
        print("PERDISTE, el numero correcto es: ",numAleat)
		
#Llamada a la función principal
main()